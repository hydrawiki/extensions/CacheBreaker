<?php
/**
 * CacheBreaker
 * CacheBreaker Hooks
 *
 * @package   CacheBreaker
 * @author    Alexia E. Smith, Tim Aldridge
 * @copyright (c) 2014 Curse Inc.
 * @license   GPL-3.0-only
 * @link      https://gitlab.com/HydraWiki/extensions/CacheBreaker
 **/

namespace CacheBreaker;

use ApiQueryImageInfo;
use ApiQueryPageImages;
use ApiQuerySiteInfo;
use ApiResult;
use Html;
use LogicException;
use RepoGroup;
use ThumbnailImage;
use Title;

class CacheBreakerHooks {
	/**
	 * Handles adding cache breaker to $wgLogo.
	 *
	 * @param object $title     [Unused] Title
	 * @param object $article   [Unused] Article
	 * @param object $output    [Unused] Output
	 * @param object $user      [Unused] User
	 * @param object $request   [Unused] WebRequest
	 * @param object $mediaWiki [Unused] Mediawiki
	 *
	 * @return boolean	True
	 */
	public static function onBeforeInitialize(&$title = null, &$article = null, &$output = null, &$user = null, $request = null, $mediaWiki = null) {
		global $wgLogo, $wgLogoHD, $wgFavicon;

		$wgLogo = self::appendHashToUrl($wgLogo);
		if (is_array($wgLogoHD)) {
			foreach ($wgLogoHD as $key => $url) {
				$wgLogoHD[$key] = self::appendHashToUrl($url);
			}
		}
		$wgFavicon = self::appendHashToUrl($wgFavicon);

		return true;
	}

	/**
	 * Handles adding cache breaker to $wgLogo.
	 *
	 * @param Title $title Title Object
	 * @param array $urls  URLs to be purged.
	 *
	 * @return boolean True
	 */
	public static function onTitleSquidURLs(Title $title, array &$urls) {
		foreach ($urls as $url) {
			if (strpos($url, 'https://') === 0) {
				$urls[] = str_replace('https://', 'http://', $url);
			}
			if (strpos($url, 'http://') === 0) {
				$urls[] = str_replace('http://', 'https://', $url);
			}
		}
		$urls = array_unique($urls);

		return true;
	}

	/**
	 * Handles adding cache breaker to API requests
	 *
	 * @param module $module API module
	 *
	 * @return boolean True
	 */
	public static function onAPIQueryAfterExecute(&$module) {
		if (!($module instanceof ApiQueryImageInfo
		|| $module instanceof ApiQueryPageImages
		|| $module instanceof ApiQuerySiteInfo
		)) {
			return true;
		}

		$params = $module->extractRequestParams(false);
		if (empty($params['prop'])) {
			return true;
		}

		$result = $module->getResult();
		$moduleHandlers = [
			'siteinfo' => ['CacheBreaker\CacheBreakerHooks', 'siteInfoHandler']
		];
		if (isset($moduleHandlers[$module->getModuleName()])) {
			call_user_func($moduleHandlers[$module->getModuleName()], $result);
		}

		return true;
	}

	/**
	 * Handle Site Info Cache
	 *
	 * @param ApiResult $result
	 *
	 * @return void
	 */
	private static function siteInfoHandler(ApiResult $result) {
		$data = $result->getResultData();
		$images = ['logo', 'favicon'];
		foreach ($images as $image) {
			if (!isset($data['query']['general'][$image])) {
				continue;
			}
			$result->addValue(
				[
					'query',
					'general'
				],
				$image,
				self::appendHashToUrl($data['query']['general'][$image]),
				ApiResult::OVERRIDE
			);
		}
	}

	/**
	 * Handles appending hashes to URLs.
	 *
	 * @param string $url       URL to Modify
	 * @param string $cacheHash Attribute to append in a GET parameter format.  Example: 'version=abcd1234'
	 *
	 * @return void
	 */
	private static function appendHash($url, $cacheHash) {
		if (!strlen($url)) {
			return $url;
		}

		$questionPos = strpos($url, '?');
		$ampPos = strrpos($url, '&');
		$anchorPos = strpos($url, '#');
		if ($questionPos !== false) {
			if ($anchorPos != $questionPos + 1) {
				if (($ampPos === false && $questionPos != strlen($url) - 1) || ($ampPos !== false && $ampPos != $questionPos + 1 && $ampPos > $questionPos)) {
					$cacheHash .= '&';
				}
			}
			$url = str_replace('?', '?' . $cacheHash, $url);
		} else {
			if ($anchorPos !== false) {
				$url = str_replace('#', '?' . $cacheHash . '#', $url);
			} else {
				$url .= '?' . $cacheHash;
			}
		}

		return $url;
	}

	/**
	 * Append the cache hash to the URL based on the file name.
	 *
	 * @param string  $url    Original URL
	 * @param integer $width  [Optional] Specify width instead of detecting it.
	 * @param integer $height [Optional] Specify height instead of detecting it.
	 *
	 * @return string Modified URL or the original URL on error.
	 */
	private static function appendHashToUrl(string $url, ?int $width = null, ?int $height = null) {
		$localRepo = null;
		try {
			$localRepo = RepoGroup::singleton()->getLocalRepo();
		} catch (LogicException $e) {
			// Configuration error with $wgLocalFileRepo.
		}
		if ($localRepo === null) {
			return $url;
		}

		$matches = [];
		if (preg_match("#/([a-z0-9])/([a-z0-9]{2})/(\w+\.\w+)(/\d+px-\w+\.\w+)?#", $url, $matches) > 0) {
			$md5_1 = $matches[1];
			$md5_2 = $matches[2];
			$file = urldecode($matches[3]);
			$fileMD5 = md5($file);
		} else {
			return $url;
		}

		if (substr($fileMD5, 0, 1) == $md5_1 && substr($fileMD5, 0, 2) == $md5_2) {
			$_file = trim($file);
			$title = Title::newFromText('File:' . $_file);
			if ($title !== null) {
				$_file = $localRepo->newFile($title);
			}

			if ($_file !== null && $_file->exists()) {
				$cacheHash = 'version=' . md5($_file->getTimestamp() . ($width !== null ? $width : $_file->getWidth()) . ($height !== null ? $height : $_file->getHeight()));

				return self::appendHash($url, $cacheHash);
			}
		}
		return $url;
	}
}
